import XCTest

import OAuthProviderTests

var tests = [XCTestCaseEntry]()
tests += OAuthProviderTests.allTests()
XCTMain(tests)
