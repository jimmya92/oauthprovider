import OAuthCommon

public protocol TokenRequest {
    
    var grantType: GrantType { get }
    var username: String? { get }
    var password: String? { get }
    var refreshToken: String? { get }
    var clientId: String { get }
    var clientSecret: String { get }
    var scopes: [Scope] { get }
}
