public enum GrantType: String, Codable {
    case password
    case refreshToken = "refresh_token"
    case clientCredentials = "client_credentials"
}
