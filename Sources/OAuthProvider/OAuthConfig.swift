import Vapor
import OAuthCommon

public struct OAuthConfig: Service {
    let clients: [ClientConfig]
    public let jwtConfig: JWTConfig
    
    public init(clients: [ClientConfig], jwtConfig: JWTConfig) {
        self.clients = clients
        self.jwtConfig = jwtConfig
    }
    
    public func validateRequest(request: TokenRequest) throws {
        guard let client = clients.first(where: { (config) -> Bool in
            return config.id == request.clientId && config.secret == request.clientSecret
        }) else {
            throw OAuthError.invalidClient
        }
        guard !client.validateScopes(request.scopes) else { return }
        throw OAuthError.invalidScopes
    }
    
    public func getClient(request: TokenRequest) -> ClientConfig? {
        return clients.first(where: { (config) -> Bool in
            return config.id == request.clientId && config.secret == request.clientSecret
        })
    }
}
