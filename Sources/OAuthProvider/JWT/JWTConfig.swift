import JWT
import OAuthCommon

public enum IssuerType: String {
    case password
    case clientCredentials
}

public struct JWTConfig {
    let secret: String
    public let accessTokenValidDuration: TimeInterval
    public let refreshTokenValidDuration: TimeInterval
    
    public init(secret: String, accessTokenValidDuration: TimeInterval, refreshTokenValidDuration: TimeInterval) {
        self.secret = secret
        self.accessTokenValidDuration = accessTokenValidDuration
        self.refreshTokenValidDuration = refreshTokenValidDuration
    }
    
    public func accessTokenForUserID(_ userID: UUID?, issuerType: IssuerType, scopes: [Scope]?) throws -> (token: String, expiresIn: TimeInterval) {
        let signer = JWTSigner.hs256(key: Data(secret.utf8))
        let expirationDate = Date(timeIntervalSinceNow: accessTokenValidDuration)
        let exp = ExpirationClaim(value: expirationDate)
        var sub: SubjectClaim?
        if let userID = userID {
            sub = SubjectClaim(value: userID.uuidString)
        }
        let iss = IssuerClaim(value: issuerType.rawValue)
        let scopes = scopes?.map { $0.identifier }
        let jwt = JWT(payload: JWTAuthorizationPayload(exp: exp, sub: sub, iss: iss, scopes: scopes))
        let data = try signer.sign(jwt)
        guard let token = String(data: data, encoding: .utf8) else {
            throw OAuthError.encoding
        }
        return (token, accessTokenValidDuration)
    }
}
